const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    userId : {
        type : String,
        required : true
    },

    products : [

            {
                productId : {
                    type : String,
                    required : true
                },

                quantity : Number,
                price : Number
            }

        ],

    totalAmount : Number,
    purchasedOn : {
        type : Date,
        default : new Date
    }
}) 

const Orders = mongoose.model("Order", orderSchema);

module.exports = Orders;