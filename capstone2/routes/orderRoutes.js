const express = require("express");

const orderControllers = require("../controllers/orderControllers.js");

const auth = require('../auth.js');

const router = express.Router();

//router for creating order
router.post('/:id', auth.verify, orderControllers.createOrder);

module.exports = router;