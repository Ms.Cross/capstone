const Products = require('../models/products.js');

const auth = require('../auth.js');


//for creating/adding product
module.exports.addProduct = (request, response) => {

	const userData = auth.decode(
		request.headers.authorization);

	   let newProduct = new Products({

	   	name: request.body.name,
	   	description: request.body.description,
	   	price: request.body.price,
	   	isActive: request.body.slots
	   })

	   if(!userData.isAdmin) {
	   	return response.send(false)
	   } else {

	   	newProduct.save()
	   	.then(save => response.send(true))
	   	.catch(error => response.send(false))
	   }
}   

//for getting or retrieving all product
module.exports.getAllProducts = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if (userData.isAdmin) {
		Products.find({})
    .then (result => response.send(result))
    .catch(error => response.send(error));
	} else {
		return response.send(`You don't have access to this route.`)
	}
}

//for retrieving all active product
module.exports.getActiveProducts = (
	request, response) => {

	Products.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

//for retrieving single document

module.exports.getProduct = (request, response) => {

	const productId = request.params.id;

	Products.findOne({isActive: true, _id: productId})
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

//for update product
module.exports.updateProduct = (request, response) => {

	const userData = auth.decode(
		request.headers.authorization);

	const productId = request.params.productId;

	let updatedProduct = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	}

	if(userData.isAdmin){

		Products.findByIdAndUpdate(productId, updatedProduct)
		.then(result => response.send(true))
		.catch(error => response.send(false));
	} else{
		return response.send(false)
	}
}

//Archive Product (Admin only)
module.exports.archiveProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId
	console.log(request.params);
    
	let archiveProduct = {
		isActive: request.body.isActive
	}

		Products.findByIdAndUpdate(productId, archiveProduct)
		.then(result => {

			if(!userData.isAdmin){

				return response.send(false)

			} else {

				if(request.body.isActive === false){
					return response.send(true)
				} else {

					return response.send(true)
				}
			}
		})

}