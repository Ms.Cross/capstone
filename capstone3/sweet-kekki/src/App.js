import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';

import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Menu from './pages/Menu.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js';
import ProductView from './pages/ProductView.js';
import CheckOut from './pages/CheckOut.js';
import Dashboard from './pages/Dashboard.js';
import ProductProps from './components/ProductProps.js';

import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';

import {UserProvider} from './UserContext.js'

function App() {

  let userDetails = {};

  useEffect(()=>{

    if(localStorage.getItem('token') === null){

      userDetails = {
        id: null,
        isAdmin: null
      }
      setUser(userDetails);

    } else {

        fetch(`${process.env.REACT_APP_API_URL}/user/userDetails`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
          userDetails = {
            id: data._id,
            isAdmin: data.isAdmin
          }
        setUser(userDetails);
      })
    }

  }, [])



  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {

    localStorage.clear()

  }

  return (
   <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar/>
        <Routes>
          <Route path = "/" element = {<Home/>} />

          <Route path = "/dashboard" element = {<Dashboard/>} />

          <Route path = "/menu" element = {<Menu/>} /> 
          <Route path = "/register" element = {user.id === null ?  <Register/> : <PageNotFound/>} />

          <Route path = "/login" element = {user.id === null ? <Login/> : <PageNotFound/> } /> 

          <Route path = "/logout" element = {<Logout/>} />

          <Route path = "/products/:id" element = {<ProductView/>} />

          <Route path = "/orders/:id" element = {<CheckOut/>} />

          <Route path = "/productProps" element = {<ProductProps/>} />

          <Route path = "*" element = {<PageNotFound/>} />

        </Routes>
      </BrowserRouter>
   </UserProvider>
  );
}

export default App;
