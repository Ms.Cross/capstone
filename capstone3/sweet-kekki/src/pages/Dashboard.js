import '../App.css';

import ProductProps from '../components/ProductProps.js';

import {Container, Row, Col, Button, Table, Form} from 'react-bootstrap'

import {useState, useEffect, useContext} from 'react';

import {Link, useNavigate} from 'react-router-dom';

import Swal2 from 'sweetalert2';

export default function Dashboard(){
    
    const [isAdd, setIsAdd] = useState(true);
    const [id, setId] = useState('');
    const [products, setProducts] = useState([]);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [quantity, setQuantity] = useState ('');
    const [price, setPrice] = useState('');
    const [isDisabled, setIsDisabled] = useState(false);

    const navigate = useNavigate();
    
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/`, {
            method: "GET",
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (result => result.json())
        .then(data =>
           setProducts(data));
    })

    useEffect(() => {
        if(name !== '' && description !== '' && quantity !== '' && price !== ''){
           setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [name, description, quantity, price])
    
    function addProduct(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`,{
            method: "POST",
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                quantity: quantity,
                price: price,
            })
        })
        .then(result => result.json())
        .then(data => {
            console.log(data);
            if(data === true){

                Swal2.fire({
                    title: 'Created product successfull!',
                    icon: 'success',
                    text: 'You may create another product or manage products!'
                })
                navigate('/dashboard')
            }else{
                Swal2.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please check details'
                })
            }           
        })
    }

   function updateProduct(e){
   e.preventDefault()
   console.log(id);
   fetch(`${process.env.REACT_APP_API_URL}/products/${id}`,{
            method: "PATCH",
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                quantity: quantity,
                price: price,
            })
        })
        .then(result => result.json())
        .then(data => {
            console.log(data);
            if(data === true){

                Swal2.fire({
                    title: 'product successfully updated!',
                    icon: 'success',
                    text: 'you can manage products!'
                })
                navigate('/dashboard')
            }else{
                Swal2.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please check details'
                })
            }           
        })
   }

   function archive(status) {
     console.log(id);
     console.log(status)
     
     fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`,{
            method: "PATCH",
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                isActive : status
                })
        })
     .then(result => result.json())
        .then(data => {
            if(data === true){

                Swal2.fire({
                    title: 'product successfully archive!',
                    icon: 'success',
                    text: 'you can manage products!'
                })
                navigate('/dashboard')
            }else{
                Swal2.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please check details'
                })
            }           
        })
   }

 return (

    <Container>
        <Row>
            <Col className = 'text-center mt-5'>
               <h1 className = "dash">Admin Dashboard</h1>
               <div className = 'd-flex justify-content-center mt-3 gap-1'>
                   <Button className = "mr-3 btn-gold btn-sm" href= "#form" onClick = {() => (setIsAdd(true)) }>add New Product</Button> 
                   <Button className = "btn-brown btn-sm">Show User Orders</Button>
               </div>

               <div className = 'text-center mt-5'>
                   <Table striped bordered hover className="mt-3">
                    <thead>
                      <tr>
                        <th className = 'bg-dark text-light'>Name</th>
                        <th colSpan={5}  className = 'bg-dark text-light'>Description</th>
                        <th  className = 'bg-dark text-light'>Quantity</th>
                        <th  className = 'bg-dark text-light'>Price</th>
                        <th  className = 'bg-dark text-light'>Availability</th>
                        <th  className = 'bg-dark text-light'>Action</th>
                     </tr>
                    </thead>
                    
                    <tbody>

                    {products.map((product, index) => (
                       <ProductProps key = {product+index} productProp = {product} onUpdate = {() => {
                        setIsAdd(false)
                        setName(product.name)
                        setDescription(product.description)
                        setQuantity(product.quantity)
                        setPrice(product.price)
                        setId(product._id)
                       }} 
                        onDisable ={ () => {
                            archive(false)
                            setId(product._id)
                        }}
                        onEnable ={ () => {
                            archive(true)
                            setId(product._id)
                        }}

                        /> 
                        ))}
                    </tbody>
                  </Table>
               </div>
            </Col>
        </Row>

        <Row>
            <Col className = "col-6 mx-auto">
              {
                isAdd ?
                <>
                       <h1 className = "text-center mt-5">New Product</h1>
              <Form onSubmit = {event => addProduct(e)} id = "form" className = "mb-5">

                <Form.Group className="mb-3" controlId="validationCustom1">
        <Form.Label>Product Name</Form.Label>
        <Form.Control 
        type="text" 
        value = {name} 
        onChange = {event =>
          setName(event.target.value) 
        } 
        placeholder="Enter Product Name" />
                </Form.Group>  

                <Form.Group className="mb-3" controlId="validationCustom2">
        <Form.Label>Description</Form.Label>
        <Form.Control 
        type="text" 
        value = {description} 
        onChange = {event =>
          setDescription(event.target.value) 
        } 
        placeholder="Description" />
                </Form.Group>

                <Form.Group className="mb-3" controlId="validationCustom3">
        <Form.Label>Quantity</Form.Label>
        <Form.Control 
        type="number" 
        value = {quantity} 
        onChange = {event =>
          setQuantity(event.target.value) 
        } 
        placeholder="Enter Quantity" />
                </Form.Group>   

                <Form.Group className="mb-3" controlId="validationCustom4">
        <Form.Label>Price</Form.Label>
        <Form.Control 
        type="number" 
        value = {price} 
        onChange = {event =>
          setPrice(event.target.value) 
        } 
        placeholder="Price" />
                </Form.Group>            

                <Button variant="primary" type="submit" 
       disabled = {isDisabled} onClick = {addProduct}>
        Create Product
                </Button>

              </Form>
                </>

                :

                <>
                

                       <h1 className = "text-center mt-5">Update Product</h1>
              <Form onSubmit = {event => addProduct(e)} id = "form" className = "mb-5">

                <Form.Group className="mb-3" controlId="validationCustom1">
        <Form.Label>Product Name</Form.Label>
        <Form.Control 
        type="text" 
        value = {name} 
        onChange = {event =>
          setName(event.target.value) 
        } 
        placeholder="Enter Product Name" />
                </Form.Group>  

                <Form.Group className="mb-3" controlId="validationCustom2">
        <Form.Label>Description</Form.Label>
        <Form.Control 
        type="text" 
        value = {description} 
        onChange = {event =>
          setDescription(event.target.value) 
        } 
        placeholder="Description" />
                </Form.Group>

                <Form.Group className="mb-3" controlId="validationCustom3">
        <Form.Label>Quantity</Form.Label>
        <Form.Control 
        type="number" 
        value = {quantity} 
        onChange = {event =>
          setQuantity(event.target.value) 
        } 
        placeholder="Enter Quantity" />
                </Form.Group>   

                <Form.Group className="mb-3" controlId="validationCustom4">
        <Form.Label>Price</Form.Label>
        <Form.Control 
        type="number" 
        value = {price} 
        onChange = {event =>
          setPrice(event.target.value) 
        } 
        placeholder="Price" />
                </Form.Group>            

                <Button variant="primary" type="submit" 
       disabled = {isDisabled} onClick = {updateProduct}>
        Update Product
                </Button>

              </Form>
                </>
              }
           </Col>
        </Row>
    </Container>
    )
}