import {Button, Container, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function Banner(){

	return(
        <Container className = 'background'>
        	<Row>
        		<Col className = 'mt-5 text-center'>
        		<h1 className= 'navbrand'>Sweet Kekki</h1>
        		<p className = "text-light">Taste the sweetness of love, buy Sweet Kekki!</p>
        		<Button className = "buttons" as = {Link} to = '/menu'>Buy Now!</Button>
        		</Col>
        	</Row>
        </Container>    
		)
}