import {Container, Nav, Navbar} from 'react-bootstrap';

import {Link, NavLink} from 'react-router-dom';

import {useContext} from 'react';

import UserContext from '../UserContext.js';

import '../App.css';

export default function AppNavBar(){

  const {user} = useContext(UserContext);
  console.log(user)
  return(
    <>
     <Navbar bg="dark" variant="dark">
            <Container>
              <Navbar.Brand className='navbrand justify-content-center' as = {Link} to = '/' >Sweet Kekki</Navbar.Brand>

              <Nav className="ms-auto">
                
              {
                user.isAdmin ?  
              <Nav.Link as = {NavLink} to = '/Dashboard'>Dashboard</Nav.Link> 
               :
              <>
                <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
               
               <Nav.Link as = {NavLink} to = '/menu'>Menu</Nav.Link>
              </>
              }
                

               

            {
              user.id === null ?
              <>
                <Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>

                <Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
              </>

               :
            
                <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
              }
              </Nav>
            </Container>
          </Navbar>
    </>	
		)
}