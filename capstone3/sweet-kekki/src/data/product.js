const productData = [
		    {
		        id: "wdc001",
		        name: "kapukekki",
		        description: "Sweet pastry Cupcake made with love and care.",
		        price: 43000,
		        onOffer: true
		    },
		    {
		        id: "wdc002",
		        name: "kohi",
		        description: "Coffee with Rich and delightful aroma.",
		        price: 50000,
		        onOffer: true
		    },
		    {
		        id: "wdc003",
		        name: "Rich in Thick savory broth.",
		        price: 55000,
		        onOffer: true
		    }
		]

		export default productData;
